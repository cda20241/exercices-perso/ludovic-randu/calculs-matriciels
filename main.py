from functions import *
    
#1- Génération d'une matrice
print("1- Fonction matrice()")
mat = matrice(3, 3)
print_matrice(mat)

#2- Change la valeur à la ligne 1, colonne 2 à 1
print("2- Fonction change_valeur() ")
matrice_modifiee = change_valeur(mat, 1, 1, 4)
print_matrice(matrice_modifiee)

#3- Matrice identité
print("3- Fonction matrice_identite() ")
matrice_id = matrice_identite(4)
print_matrice(matrice_id)

#5- dimension_check() qui compare les dimensions de 2 matrices
print("5- Fonction de comparaison dimension_check()")
print(dimension_check(mat, matrice_id))

#6- Somme_matrice()
print("6- Fonction de somme de 2 matrices somme_matrice()")
try:
    matrice_somme = somme_matrice(matrice_modifiee, matrice_id)
    if matrice_somme is not False:
        print_matrice(matrice_somme)
except MatriceDimensionError as e:
    print(e)
#7- Fonction is_element() qui prends en parametre une matrice et un nombre
print("7- is_element recherche le nombre 5 sur la matrice de somme et affiche sa position")
nombre = 5
position = is_element(mat, nombre)
if position != False:
    print(f"Le nombre {nombre} se trouve à la position {position}")
else:
    print(f"Le nombre {nombre} n'a pas été trouvé dans la matrice")

#8 Produit scalaire
print("8- produit_scalaire() retourne le produit scalaire de la matrice modifiée et du nombre 2.5 ")
nombre = 2.5
resultat = produit_scalaire(matrice_modifiee, nombre)
print_matrice(resultat)

#9  produit_matriciel de la premiere matrice et de la matrice modifiée
print("8- produit_matriciel() retourne le produit de 2 matrices ")
try:
    produit_matrice = produit_matriciel(mat, matrice_modifiee)
    if produit_matrice is not False:
        print_matrice(produit_matrice)
except MatriceDimensionError as e:
    print(e)