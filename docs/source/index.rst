.. Calculs matriciels documentation master file, created by
   sphinx-quickstart on Tue Oct  3 10:00:48 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation sur les calculs matriciels
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents: Fonctions de calculs matriciels




* :ref:`search`

print_matrice()
===============
Fonction qui prends la matrice a afficher en option.
*Exemple:* **print_matrice(matrice)**

matrice()
============================
Fonction qui génère une matrice et qui prends le nombre de ligne et le nombre de colonnes en options
*Exemple:* **matrice(3, 3)** génèrera une matrice sur 3 colonnes et 3 lignes


change_valeur()
================
Fonction qui génère une matrice et qui prends le nombre de ligne et le nombre de colonnes en options
*Exemple:* **change_valeur(matrice, 1, 1, 4)** remplacera dans la matrice "matrice" le chiffre de la colonne 1, ligne une par le chiffre 4

matrice_identite()
===================
Fonction qui génère une matrice identité et qui prends le nombre de ligne et de colonnes en options
*Exemple:* **matrice(3)** génèrera une matrice identité sur 3 colonnes et 3 lignes

dimension_check()
===================
Fonction qui compare 2 matrices et qui renvoie soit True soit False
*Exemple:* **dimension_check(matrice1, matrice2)**

somme_matrice()
===================
Fonction qui additionne 2 matrices et qui renvoie une nouvelle matrice
*Exemple:* **somme_matrice(matrice1, matrice2)**

is_element()
===================
Fonction qui recherche un nombre dans une matrice.
Si le chiffre existe renvoie la position du chiffre dans la matrice sinon renvoie False
*Exemple:* **is_element(matrice, 4)**

produit_scalaire()
===================
Fonction qui retourne le produit scalaire d'une matrice et d'un chiffre
*Exemple:* **produit_scalaire(matrice, 4)**

produit_matriciel()
===================
Fonction qui retourne le produit matriciel de deux matrices.

*Exemple:* **produit_matriciel(matrice1, matrice2)**
