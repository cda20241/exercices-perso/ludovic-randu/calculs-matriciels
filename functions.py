class MatriceDimensionError(Exception):
    def __init__(self, message="Les dimensions des matrices ne sont pas identiques."):
        self.message = message
        super().__init__(self.message)
def matrice(lignes, colonnes):
    """
    Fonction qui crée une matrice
    :param lignes: Nbrs de lignes de la matrice
    :param colonnes: Nbrs de colones de la matrice
    :return: retourne la matrice
    """
    # Création d'une liste vide
    resultat = []
    # Créer la matrice avec des 0
    for i in range(lignes):
        ligne = [0] * colonnes
        resultat.append(ligne)
    return resultat


def change_valeur(matrice, ligne, colonne, nouvelle_valeur):
        """
        Fonction pour modifier la valeur d'un chiffre
        :param matrice: matrice à modifier
        :param ligne: numéro de la ligne à modifier
        :param colonne: numéro de la collone à modifier
        :param nouvelle_valeur: nouvelle valeur a affecter
        :return: renvoi la matrice
        """
        # On vérifie si la ligne et la colonne existent
        if 0 <= ligne < len(matrice) and 0 <= colonne < len(matrice[0]):
            # Modifiez la valeur à la position spécifiée dans la matrice
            matrice[ligne-1][colonne-1] = nouvelle_valeur
        else:
            print("Erreur : La ligne ou de colonne n'existe pas.")
        return matrice


def matrice_identite(taille):
    """
    :param taille: taille de la matrice désirée (2x2, 3x3 ....)
    :return: renvoi la matrice
    """
    # Création d'une matrice de taille x taille
    matrice = []
    for i in range(taille):
        ligne = [0] * taille
        matrice.append(ligne)
    for i in range(taille):
        matrice[i][i] = 1
    return matrice


def dimension_check(matrice1, matrice2):
    """
    Fonction qui compare 2 matrice sur le nombre de ligne/colonnes
    :param matrice1: première matrice
    :param matrice2: seconde matrice à comparer avec la première
    :return: True si elles sont de même taille, sinon renvoi False
    """
    if len(matrice1) != len(matrice2) or len(matrice1[0]) != len(matrice2[0]):
        return False
    else:
        return True


def print_matrice(matrice):
    """
    Fonction qui affiche la matrice ligne par ligne
    :param matrice: matrice à afficher
    :return: Affiche la matrice
    """
    for ligne in matrice:
        print (ligne)
    print('')

def somme_matrice(matrice1, matrice2):
    # Vérifiez que les matrices ont la même taille
    if dimension_check(matrice1, matrice2) == False :
        print("Erreur : Les matrices n'ont pas la même taille.")
        return False

    lignes = len(matrice1)
    colonnes = len(matrice1[0])

    # Créez une nouvelle matrice pour stocker la somme
    resultat = []

    # Parcourez les lignes et les colonnes et ajoutez les éléments correspondants
    for i in range(lignes):
        ligne_resultat = []
        for j in range(colonnes):
            valeur = matrice1[i][j] + matrice2[i][j]
            ligne_resultat.append(valeur)
        resultat.append(ligne_resultat)

    return resultat

def is_element(matrice, nbr):
    """
    Fonction qui recherche un nombre dans une matrice
    :param matrice: la matrice dans laquelle on va rechercher le chiffre
    :param nbr: chiffre à rechercher dans la matrice
    :return: si le chiffre existe renvoie la position du chiffre dans la matrice sinon renvoie False
    """
    for x in range(len(matrice)):
        for y in range(len(matrice[0])):
            if matrice[x][y] == nbr:
                return (x, y)
    return False

def produit_scalaire(matrice, nombre):
    """
    Fonction qui retourne le produit scalaire d'une matrice et d'un chiffre
    :param matrice:
    :param nombre:
    :return:
    """
    resultat = []
    for ligne in matrice:
        ligne_resultat = [element * nombre for element in ligne]
        resultat.append(ligne_resultat)
    return resultat




def produit_matriciel(matrice1, matrice2):
    """

    :param matrice1:
    :param matrice2:
    :return:
    """
    # Vérifier si les dimensions sont compatibles pour le produit matriciel
    if dimension_check(matrice1, matrice2) == False:
        raise MatriceDimensionError()

    # Initialiser la matrice résultante avec des zéros
    lignes_resultat = len(matrice1)
    colonnes_resultat = len(matrice2[0])
    resultat = [[0 for _ in range(colonnes_resultat)] for _ in range(lignes_resultat)]

    # Effectuer le produit matriciel
    for i in range(lignes_resultat):
        for j in range(colonnes_resultat):
            for k in range(len(matrice2)):
                resultat[i][j] += matrice1[i][k] * matrice2[k][j]

    return resultat